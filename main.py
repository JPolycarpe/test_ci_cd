"""This is the main"""

from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    """Hello world!"""
    return 'Hello world!'
